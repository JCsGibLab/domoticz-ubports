import QtQuick 2.4
import QtQuick.Layouts 1.1
import Ubuntu.Components 1.3

Page {
  anchors.fill: parent
  visible: false

  onVisibleChanged: {
    if ((visible) && (settings.baseUrl != "")) {
        if (debug) {console.log("DeviceList: Visibility changed: Get devices")}
        apiFct.getDevices(currentUrl, false)
    } else {
      if (debug) {console.log("DeviceList: Visibility changed: Not connected")}
    }
    if (settings.myAuth != "") {
      apiFct.setAuthorization("Basic " + settings.myAuth)
    }
  }

  property var fontSize: fontSze
  property var currentUrl: baseDomoticzUrl+"/json.htm?type=lightlog&idx="+myIdx
  // property var textLogUrl: baseDomoticzUrl+"/json.htm?type=textlog&idx="+myIdx
  // property var counterLogUrl: baseDomoticzUrl+"/json.htm?type=graph&sensor=counter&idx="+myIdx+"&range=month"

  property var name
  property var myIdx

  header: PageHeader {
    id: header
    title: i18n.tr('History - ')+name
    trailingActionBar {
      actions: [
      Action {
        iconName: "info"
        onTriggered: Qt.openUrlExternally("https://www.domoticz.com/")
      }
      ]
    }
  } // PageHeader

  Rectangle { // List of events
    id: deviceList
    anchors.top: header.bottom
    anchors.leftMargin: units.gu(1)
    anchors.left: parent.left
    anchors.bottom: parent.bottom
    width: parent.width-units.gu(1)
    height: parent.height - header.height

    Component {
      id: devDelegate

      ListItem {
        height: units.gu(7)

        Column {
          id: currentDev
          anchors.centerIn: parent
          width: parent.width

          Rectangle { // Date
            width: parent.width
            height: units.gu(3)
            Text { anchors.centerIn: parent; text: logdate; font.pixelSize: fontSize ; width: parent.width }
          } // Rectangle: Date

          Row {
            spacing: units.gu(1)

            Rectangle { // Status
              width: units.gu(30)
              height: units.gu(3)
              Text { anchors.centerIn: parent; text: status; font.pixelSize: fontSize ; width: parent.width }
            } // Rectangle: Status

            Rectangle { // Level
              width: units.gu(10)
              height: units.gu(3)
              Text { anchors.centerIn: parent; text: level; font.pixelSize: fontSize ; width: parent.width }
            } // Rectangle: Level
          }
        } // Column
      } // ListItem
    } // Component : delegate

    UbuntuListView {
      id: testlist
      anchors.fill: parent
      currentIndex: -1
      model: ListModel { id: devModel }
      delegate: devDelegate
      pullToRefresh {
        id: refresher
        enabled: true
        onRefresh: apiFct.getDevices(currentUrl, true)
      }
    } // UbuntuListView
  } // Rectangle : List of switches

  // Utility functions
  Functions {id: myFct}
  Requests {id: apiFct}

} // Page

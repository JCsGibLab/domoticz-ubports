import QtQuick 2.4
import QtQuick.Layouts 1.1
import Ubuntu.Components 1.3

Page {
  anchors.fill: parent
  visible: false

  onVisibleChanged: {
    if ((visible) && (settings.baseUrl != "")) {
        if (debug) {console.log("DeviceList: Visibility changed: Get devices")}
        apiFct.getDevices(currentUrl, false)
    } else {
      if (debug) {console.log("DeviceList: Visibility changed: Not connected")}
    }
    if (settings.myAuth != "") {
      apiFct.setAuthorization("Basic " + settings.myAuth)
    }
  }

  property var fontSize: mainFontSize
  property var dashboardUrl: baseDomoticzUrl+"/json.htm?type=devices&used=true&filter=all&favorite=1&order=Name"
  property var switchesUrl: baseDomoticzUrl+"/json.htm?type=devices&filter=light&used=true&order=Name"
  property var scenesUrl: baseDomoticzUrl+"/json.htm?type=scenes"
  property var temperatureUrl: baseDomoticzUrl+"/json.htm?type=devices&filter=temp&used=true&order=Name"
  property var weatherUrl: baseDomoticzUrl+"/json.htm?type=devices&filter=weather&used=true&order=Name"
  property var utilityUrl: baseDomoticzUrl+"/json.htm?type=devices&filter=utility&used=true&order=Name"

  property var currentUrl: dashboardUrl
  property var currentPage: 0

  header: PageHeader {
    id: header
    leadingActionBar {
      actions: [
      Action {
        iconName: "tag"
        text: i18n.tr('User variables')
        onTriggered: stack.push(uservar)
      },
      Action {
        iconName: "settings"
        text: i18n.tr('Parameters')
        onTriggered: stack.push(parameters)
      }
      ]
    }
    title: i18n.tr('Dashboard')
    trailingActionBar {
      actions: [
      Action {
        iconName: "info"
        onTriggered: Qt.openUrlExternally("https://www.domoticz.com/")
      }
      ]
    }
  } // PageHeader

  Rectangle { // List of switches
    id: deviceList
    anchors.top: header.bottom
    anchors.leftMargin: units.gu(1)
    anchors.left: parent.left
    anchors.bottom: foot.top
    width: parent.width-units.gu(1)
    height: parent.height - header.height

    Component {
      id: devDelegate

      ListItem {
        height: units.gu(6)
        trailingActions: ListItemActions {
          actions: [
          Action {
            iconName: "filters"
            onTriggered: {
              advanced.myIdx = model.idx
              advanced.slider.maximumValue = model.maxLevel
              advanced.details.text = model.devData
              advanced.info.text = model.name
              advanced.mySelectorList = model.switchOptions
              advanced.myType = model.type
              advanced.myLightColor = model.lightColor
              advanced.slider.value = model.level
              stack.push(advanced)
            }
          },
          Action {
            visible: (isSwitch != '')
            iconName: "history"
            onTriggered: {
              lightlogs.myIdx = model.idx
              lightlogs.name = model.name
              stack.push(lightlogs)
            }
          },
          Action {
            visible: (type == 'temperature')
            iconName: "history"
            onTriggered: {
              templogs.myIdx = model.idx
              templogs.name = model.name
              stack.push(templogs)
            }
          },
          Action {
            iconName: (model.favorite) ? "starred" : "non-starred"
            onTriggered: {
              var fav = (model.favorite)? 0 : 1
              apiFct.callRequest(baseDomoticzUrl+"/json.htm?type=command&param=makefavorite&idx="+idx+"&isfavorite="+fav)
            }
          }
          ]
        } // trailingActions

        Row {
          id: currentDev
          anchors.centerIn: parent
          spacing: units.gu(1)

          // Scene activation
          property var sceneOn: baseDomoticzUrl+"/json.htm?type=command&param=switchscene&idx="+idx+"&switchcmd=On"

          // Switches actions
          property var switchOn: baseDomoticzUrl+"/json.htm?type=command&param=switchlight&idx="+idx+"&switchcmd=On"
          property var switchOff: baseDomoticzUrl+"/json.htm?type=command&param=switchlight&idx="+idx+"&switchcmd=Off"

          Icon {
            visible: ((type != "temperature") && ((currentPage!=3) || (temperature == 999)))
            width: units.gu(5)
            height: units.gu(5)
            name:   myFct.getDeviceIconName (type, is.checked)
            color:  myFct.getDeviceIconColor(type, is.checked)
          }
          Icon {
            visible: ((type == 'temperature')||((currentPage==3) && temperature != 999))
            width: units.gu(5)
            height: units.gu(5)
            source: "../assets/temp.svg"
            color: myFct.getTemperatureColor(temperature)
          }

          Rectangle {
            width: units.gu(27)
            height: units.gu(5)
            Text { anchors.centerIn: parent; text: name; font.pixelSize: fontSize ; width: parent.width }
          }

          Rectangle {
            width: units.gu(12)
            height: units.gu(5)

            Switch { id: is; visible: ((currentPage == 1)||(isSwitch != '')); anchors.centerIn: parent; checked: (status == "Off") ? false : true
              onClicked: {
                if (checked) {
                  if (debug) {console.log("Switch turned on")}
                  apiFct.callRequest(currentDev.switchOn)
                } else {
                  if (debug) {console.log("Switch turned off")}
                  apiFct.callRequest(currentDev.switchOff)
                }
              }
            }
            Icon {
              visible: ((type == 'Scene') || (currentPage==2))
              anchors.centerIn: parent
              width: units.gu(5)
              height: units.gu(5)
              name: "system-shutdown"
              color: UbuntuColors.blue
              MouseArea {anchors.fill: parent; onClicked: apiFct.callRequest(currentDev.sceneOn)}
            }
            Text { visible: ((type == 'temperature') || ((currentPage == 3) && (temperature != 999))); // (((currentPage == 2)||(currentPage == 0)) && (type == 'temperature'));
              anchors.centerIn: parent; text: temperature+" °C"; font.pixelSize: fontSize; font.bold: true
            }
            Text { visible: ((type != 'temperature') && ((temperature == 999)||(currentPage == 4)||(currentPage == 0)) && (meteoData != ""));
              anchors.centerIn: parent; text: meteoData ; font.pixelSize: fontSize; font.bold: (type != "forecast")
            }
            Text { visible: ((meteoData == '') && (type != 'temperature') && (type != 'Scene') && (isSwitch == ''));
              anchors.centerIn: parent; text: devData ; font.pixelSize: fontSize; font.bold: (type != "forecast")
            }
          } // Rectangle : type specific field

        } // Row
      } // ListItem
    } // Component : delegate

    UbuntuListView {
      id: testlist
      anchors.fill: parent
      currentIndex: -1
      model: ListModel { id: devModel }
      delegate: devDelegate
      pullToRefresh {
        id: refresher
        enabled: true
        onRefresh: apiFct.getDevices(currentUrl, true)
      }
    } // UbuntuListView
  } // Rectangle : List of switches

  Footer {id: foot}

  // Function used to refresh page content depending on the data type expected
  function refresh(pageIndex) {
    currentPage = pageIndex
    switch (pageIndex) {
      case 0:
      header.title = i18n.tr('Dashboard')
      currentUrl = dashboardUrl
      break
      case 1:
      header.title = i18n.tr('Switches')
      currentUrl = switchesUrl
      break
      case 2:
      header.title = i18n.tr('Scenes')
      currentUrl = scenesUrl
      break
      case 3:
      header.title = i18n.tr('Temperature')
      currentUrl = temperatureUrl
      break
      case 4:
      header.title = i18n.tr('Weather')
      currentUrl = weatherUrl
      break
      case 5:
      header.title = i18n.tr('Utility')
      currentUrl = utilityUrl
    }

    apiFct.getDevices(currentUrl, false)
  }

  // Utility functions
  Functions {id: myFct}
  Requests {id: apiFct}

} // Page

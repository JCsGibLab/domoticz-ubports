import QtQuick 2.9
import QtQuick.Layouts 1.1
import Qt.labs.settings 1.0
import Ubuntu.Components 1.3

MainView {
  id: root

  objectName: 'mainView'
  applicationName: 'domoticz.applee'
  automaticOrientation: true

  width: units.gu(47)
  height: units.gu(75)

  property var debug: true
  property var mainFontSize: units.gu(2)
  property alias deviceList: devList
  property alias stack: myPageStack
  property alias params: parameters
  property alias uservar: uservariables
  property alias editvar: editvariables
  property alias advanced: slider
  property alias templogs: logtemp
  property alias lightlogs: loglight
  // property alias name

  Settings {
    id: settings
    property var protocol: ""
    property var baseUrl: ""
    property var basePort: ""
    property var myAuth: ""
  }
  // Base url for domoticz
  property var baseDomoticzUrl: settings.protocol + "://" + settings.baseUrl + ":" + settings.basePort

  PageStack {
    id: myPageStack

    Component.onCompleted: {
      myPageStack.push(devList)
      if (settings.baseUrl == "") {
        myPageStack.push(parameters)
      }
    }

    DeviceList {
      id: devList
    } // DeviceList

    PageSettings {
      id: parameters
    } // PageSettings

    PageUserVar {
      id: uservariables
    } // PageUserVar

    EditUserVar {
      id: editvariables
    } // PageUserVar

    Advanced {
      id: slider
    } // Advanced

    TempLog {
      id: logtemp
    } // TempLog

    LightLog {
      id: loglight
    } // LightLog

  } // PageStack
} // MainView

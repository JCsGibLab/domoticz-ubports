import QtQuick 2.4
import QtQuick.Layouts 1.1
import Ubuntu.Components 1.3

Page {
  anchors.fill: parent
  id: thisPage
  visible: false

  property var varType
  property var varValue
  property var varName

  onVarTypeChanged: {
    console.log("EditUserVar: type changed "+varType);
    type.text = i18n.tr('Variable type: ')

    // Depending on type, set placeholder and input method
    if (varType == 0) {
      type.text += i18n.tr('Integer')
      vValue.placeholderText = i18n.tr('Input an integer')
      vValue.inputMethodHints = Qt.ImhDigitsOnly
      myValidator.regExp = /^\d+$/
    } else if (varType == 1) {
      type.text += i18n.tr('Float')
      vValue.placeholderText = i18n.tr('Input a float')
      vValue.inputMethodHints = Qt.ImhDigitsOnly
      myValidator.regExp = /^\d+\.?\d*$/
    } else if (varType == 2) {
      type.text += i18n.tr('String')
      vValue.placeholderText = i18n.tr('Input a string')
      vValue.inputMethodHints = Qt.ImhNone
      myValidator.regExp = /^.*$/
    } else if (varType == 3) {
      type.text += i18n.tr('Date')
      vValue.placeholderText = i18n.tr('DD/MM/YYYY')
      vValue.inputMethodHints = Qt.ImhPreferNumbers
      myValidator.regExp = /^\d{2}\/\d{2}\/\d{4}$/
    } else if (varType == 4) {
      type.text += i18n.tr('Time')
      vValue.placeholderText = i18n.tr('HH:MM')
      vValue.inputMethodHints = Qt.ImhPreferNumbers
      myValidator.regExp = /^\d{2}:\d{2}$/
    }

    // Set the validator for the current type
    vValue.validator = myValidator
    typeSelector.selectedIndex = varType
  }

  onVarNameChanged: {
    name.text = i18n.tr("Variable name: ")
    name.text += varName
  }

  onVarValueChanged: {
    vValue.text = varValue
  }

  header: PageHeader {
    id: header
    title: i18n.tr('Edit variable')
  } // PageHeader

  Rectangle {
    id: advancedPage
    anchors.top: header.bottom
    anchors.leftMargin: units.gu(2)
    anchors.left: parent.left
    width: parent.width-units.gu(2)
    height: parent.height - header.height

    Column {
      anchors.fill: parent
      spacing: units.gu(2)

      Rectangle { // Device name
        height: units.gu(5); width: parent.width
        Label { anchors.centerIn: parent; width: parent.width; id: name }
      } // Rectangle: Device name

      OptionSelector {
        id: typeSelector
          text: i18n.tr('Variable type: ')
          model: [i18n.tr("Integer"),
                  i18n.tr("Float"),
                  i18n.tr("String"),
                  i18n.tr("Date"),
                  i18n.tr("Time")
                  ]
          onDelegateClicked: {
            console.log("EditUserVar: type selected "+index);
            varType = index
          }
      }

      Rectangle { // Device's state
        height: units.gu(5); width: parent.width
        Label { anchors.centerIn: parent; width: parent.width; id: type }
      } // Rectangle: Devices's state

      TextField {
        id: vValue
      } // TextField: Input field for user variable

      Rectangle { // OK Cancel buttons
        id: okcancel
        width : parent.width
        height: units.gu(5)

        Row {
          id: row
          anchors.centerIn: parent
          spacing: units.gu(1)

          Button {
            text: i18n.tr("OK")
            enabled: vValue.acceptableInput
            color: UbuntuColors.green
            onClicked: {
              apiFct.callRequest(baseDomoticzUrl+"/json.htm?type=command&param=updateuservariable&vname="+varName+"&vtype="+varType+"&vvalue="+vValue.text)
              stack.pop()
            }
          } // Button: OK

          Button {
            text: i18n.tr("Cancel")
            onClicked: {
              stack.pop()
            }
          } // Button: Cancel
        } // Row
      } // Rectangle: OK CANCEL

    } // Column
  } // Rectangle

  RegExpValidator {
    id: myValidator
  } // RegExpValidator: validator for variable type

  // Utility functions
  Requests {id: apiFct}

} // Page

import QtQuick 2.4
import QtQuick.Layouts 1.1

Rectangle {
  id: colorPicker

  property color colorValue: _hsla(hueSlider.value, svPicker.saturation, svPicker.brightness, 1)
  property color oldColorValue: "black"
  property int colorHandleRadius : 8
  property var previousColor
  property var hexColor

  Rectangle {
    id: colorzone
    width : parent.width
    height: units.gu(22)

    Row {
      id: row
      spacing: units.gu(1)

      // saturation/brightness picker box
      SBPicker {
        id: svPicker

        width: units.gu(20)
        height: units.gu(20)

        hueColor: {
          var v = 1.0-hueSlider.value

          if(0.0 <= v && v < 0.16) {
            return Qt.rgba(1.0, 0.0, v/0.16, 1.0)
          } else if(0.16 <= v && v < 0.33) {
            return Qt.rgba(1.0 - (v-0.16)/0.17, 0.0, 1.0, 1.0)
          } else if(0.33 <= v && v < 0.5) {
            return Qt.rgba(0.0, ((v-0.33)/0.17), 1.0, 1.0)
          } else if(0.5 <= v && v < 0.67) {
            return Qt.rgba(0.0, 1.0, 1.0 - (v-0.5)/0.17, 1.0)
          } else if(0.67 <= v && v < 0.83) {
            return Qt.rgba((v-0.67)/0.16, 1.0, 0.0, 1.0)
          } else if(0.83 <= v && v <= 1.0) {
            return Qt.rgba(1.0, 1.0 - (v-0.83)/0.17, 0.0, 1.0)
          } else {
            console.log("ColorPicker: Hue value is out of expected boundaries of [0, 1]")
            return "red"
          }
        }
      } // Saturation Value Picker

      // hue picking slider
      Item {
        id: huePicker
        width: units.gu(2)
        height: units.gu(20)

        // Layout.alignment: Qt.AlignLeft | Qt.AlignTop
        // Layout.fillHeight: true
        Rectangle {
          anchors.fill: parent
          id: colorBar
          gradient: Gradient {
            GradientStop { position: 1.0;  color: "#FF0000" }
            GradientStop { position: 0.83; color: "#FFFF00" }
            GradientStop { position: 0.67; color: "#00FF00" }
            GradientStop { position: 0.5;  color: "#00FFFF" }
            GradientStop { position: 0.33; color: "#0000FF" }
            GradientStop { position: 0.16; color: "#FF00FF" }
            GradientStop { position: 0.0;  color: "#FF0000" }
          }
        }
        ColorSlider {
          id: hueSlider; anchors.fill: parent
        }
      } // Hue picker

      Column {
        width: units.gu(20)
        spacing: units.gu(2)
        Row {
          height: units.gu(8)
          spacing: units.gu(1)
          // H, S, V color values boxes
          Column {
            width: units.gu(9)
            height: parent.height
            spacing: units.gu(1)
            NumberBox { caption: "H:"; value: hueSlider.value.toFixed(2) }
            NumberBox { caption: "S:"; value: svPicker.saturation.toFixed(2) }
            NumberBox { caption: "V:"; value: svPicker.brightness.toFixed(2) }
          }

          // R, G, B color values boxes
          Column {
            width: units.gu(9)
            height: parent.height
            spacing: units.gu(1)
            NumberBox { caption: "R:"; value: _getChannelStr(colorPicker.colorValue, 0); min: 0; max: 255 }
            NumberBox { caption: "G:"; value: _getChannelStr(colorPicker.colorValue, 1); min: 0; max: 255 }
            NumberBox { caption: "B:"; value: _getChannelStr(colorPicker.colorValue, 2); min: 0; max: 255 }
          }
        }

        // "#XXXXXX" color value box
        Rectangle {
          id: colorEditBox
          height: units.gu(3)
          width: units.gu(20)
          border.width: units.gu(0.2)

          TextInput {
            id: hexInputField
            anchors.centerIn: parent
            color: "#AAAAAA"
            selectionColor: "#7777AA"
            font.pointSize: units.gu(1.4)
            maximumLength: 9
            focus: false
            text: "#"+hexColor
            selectByMouse: true
          }
        }

        Rectangle {
          border.width: 1
          border.color: "#AAAAAA"
          width: units.gu(20)+2
          height: units.gu(5)+2
          color: colorPicker.colorValue
          Row {
            anchors.fill: parent
            // height: parent.height
            Rectangle {
              width: units.gu(10)
              height: parent.height-2
              color: colorPicker.colorValue
            }
            Rectangle {
              width: units.gu(10)
              height: parent.height-2
              color: colorPicker.oldColorValue
            }
          }
        } // Display changing color
      } // Color information
    } // Row
  } // Rectangle

  onPreviousColorChanged: {
    if (debug) {console.log("ColorPicker: Previous color="+previousColor)}

    var col = eval(previousColor)
    if (col) {
      var r= col.r.toString(16)
      r= (r.length==1)?"0"+r : r
      var g= col.g.toString(16)
      g= (g.length==1)?"0"+g : g
      var b= col.b.toString(16)
      b= (b.length==1)?"0"+b : b

      if (debug) {console.log("HEX="+oldColorValue+" R="+col.r+" G="+col.g+" B="+col.b+" M="+col.m+" CW="+col.cw+" WW="+col.ww)}
      hexColor = r+g+b;
      oldColorValue = "#"+hexColor;
    }
    // If mode white => nothing to do
    // If mode RGB => compute HSV values and update
  }

  //  creates color value from hue, saturation, brightness, alpha
  function _hsla(h, s, b, a) {
    var lightness = (2 - s)*b
    var satHSL = s*b/((lightness <= 1) ? lightness : 2 - lightness)
    lightness /= 2

    var c = Qt.hsla(h, satHSL, lightness, a)

    hexColor = (c.toString().substr(1, 6)).toUpperCase()

    return c
  }

  //  extracts integer color channel value [0..255] from color value
  function _getChannelStr(clr, channelIdx) {
    return parseInt(clr.toString().substr(channelIdx*2 + 1, 2), 16)
  }
}

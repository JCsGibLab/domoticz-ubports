import QtQuick 2.4
import QtQuick.Layouts 1.1
import Ubuntu.Components 1.3

Page {
  anchors.fill: parent
  visible: false

  onVisibleChanged: {
    if ((visible) && (settings.baseUrl != "")) {
        if (debug) {console.log("DeviceList: Visibility changed: Get devices")}
        apiFct.getDevices(currentUrl, false)
    } else {
      if (debug) {console.log("DeviceList: Visibility changed: Not connected")}
    }
    if (settings.myAuth != "") {
      apiFct.setAuthorization("Basic " + settings.myAuth)
    }
  }

  property var fontSize: fontSze
  property var currentUrl: baseDomoticzUrl+"/json.htm?type=graph&sensor=temp&idx="+myIdx+"&range=year"

  property var name
  property var myIdx

  header: PageHeader {
    id: header
    title: i18n.tr('History - ')+name
    trailingActionBar {
      actions: [
      Action {
        iconName: "info"
        onTriggered: Qt.openUrlExternally("https://www.domoticz.com/")
      }
      ]
    }
  } // PageHeader

  Rectangle { // List of events
    id: deviceList
    anchors.top: header.bottom
    anchors.leftMargin: units.gu(1)
    anchors.left: parent.left
    anchors.bottom: parent.bottom
    width: parent.width-units.gu(1)
    height: parent.height - header.height

    Component {
      id: devDelegate

      ListItem {
        height: units.gu(7)

        Column {
          id: currentDev
          anchors.centerIn: parent
          width: parent.width

          Rectangle { // Date
            width: parent.width
            height: units.gu(3)
            Text { anchors.centerIn: parent; text: logdate; font.pixelSize: fontSize ; width: parent.width }
          } // Rectangle: Date

          Row {
            spacing: units.gu(1)
            Rectangle { // T Min
              width: units.gu(15)
              height: units.gu(3)
              Row {
                Text { text: i18n.tr("Min "); font.pixelSize: fontSize }
                Text { text: minTemp; color: UbuntuColors.blue; font.pixelSize: fontSize }
                Text { text: "/"+minTempOld; font.pixelSize: fontSize }
              }
            } // Rectangle: T Min

            Rectangle { // T Avg
              width: units.gu(15)
              height: units.gu(3)
              Row {
                Text { text: i18n.tr("Avg "); font.pixelSize: fontSize }
                Text { text: avgTemp; color: UbuntuColors.ash; font.pixelSize: fontSize }
                Text { text: "/"+avgTempOld; font.pixelSize: fontSize }
              }
            } // Rectangle: T Avg

            Rectangle { // T Max
              width: units.gu(15)
              height: units.gu(3)
              Row {
                Text { text: i18n.tr("Max "); font.pixelSize: fontSize }
                Text { text: maxTemp; color: UbuntuColors.red; font.pixelSize: fontSize }
                Text { text: "/"+maxTempOld; font.pixelSize: fontSize }
              }
            } // Rectangle: T Max
          } // Row

        } // Column
      } // ListItem
    } // Component : delegate

    UbuntuListView {
      id: testlist
      anchors.fill: parent
      currentIndex: -1
      model: ListModel { id: devModel }
      delegate: devDelegate
      pullToRefresh {
        id: refresher
        enabled: true
        onRefresh: apiFct.getDevices(currentUrl, true)
      }
    } // UbuntuListView
  } // Rectangle : List of switches

  // Utility functions
  Functions {id: myFct}
  Requests {id: apiFct}

} // Page

#ifndef WEBREQUESTPLUGIN_H
#define WEBREQUESTPLUGIN_H

#include <QQmlExtensionPlugin>

class WebrequestPlugin : public QQmlExtensionPlugin {
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "org.qt-project.Qt.QQmlExtensionInterface")

public:
    void registerTypes(const char *uri);
};

#endif

# Domoticz

This QML app allows you to control Domoticz home automation services from your UT phone.
The app is work in progress but the basic features are available :
*  Turn switches on and off
*  Dimmable lights can be set to any percentage thanks to a slider
*  RGB lights can be set to any RGB value
*  Devices data are displayed (tested for temperatures, level intensity, switch state, power meters and meteo)

Please help me improve and don't hesitate to open an issue here on Gitlab.
It can take time to solve, but I will look into it.

Many thanks to Rshest and his qml-colorpicker, this set the basis on which I built the advanced parameters : 
https://github.com/rshest/qml-colorpicker

Download the app for UBPorts on the open store:
[![OpenStore](https://open-store.io/badges/en_US.svg)](https://open-store.io/app/domoticz.applee)